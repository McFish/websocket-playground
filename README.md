Testing playground for pm2 process management, nats message queue and websockets.

# "development"-environment

* Start frontend and backend individually from their folders with command: docker compose up
* Open browser in address http://localhost:8080
* Make sure the address is: ws://localhost:8000 then click 'connect'
* Type something the message box and press 'send'

# "production"-environment

* Start up the stack in root folder with command: docker compose up
* Open browser in address http://localhost:8080
* Make sure the address is: ws://localhost:8080/api then click 'connect'
* Type something the message box and press 'send'

Note that the address for websocket is different in development and production environments

# Backend clustering

To try out clustering, open up another tab in browser and repeat the above instructions. In the docker console and in browser, you should see responses from another process. The number between square brackets is the instance number (either 0 or 1).

