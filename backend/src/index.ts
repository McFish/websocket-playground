import express, { Application } from 'express';
import { connect, NatsConnection, StringCodec } from 'nats';
import cors from 'cors';
import ws from 'ws';

const port = process.env.PORT || 8000;
const natsUri = "nats:4222"

// Connect to nats
let natcConn: NatsConnection | null = null;
const sc = StringCodec();

(async () => {
  natcConn = await connect({ servers: natsUri });
  console.log(`[${process.env.NODE_APP_INSTANCE}] Connected to nats`);
  const sub = natcConn.subscribe('ws.broadcast');
  for await (const m of sub) {
    console.log(`[${process.env.NODE_APP_INSTANCE}] Nats message: ${sc.decode(m.data)}`);
  }
})();

// Create websocket endpoint
const wsServer = new ws.Server({ noServer: true });
wsServer.on('connection', socket => {
  socket.on('message', message => {

    socket.send(`[${process.env.NODE_APP_INSTANCE}] ACK message: ${message}`);
    natcConn.publish('ws.broadcast', sc.encode(message.toString()));

    console.log(`[${process.env.NODE_APP_INSTANCE}] Websocket message: ${message}`);
  });
});

// Create express app
const app: Application = express();
app.use(cors());
const server = app.listen(port);

// Handle websocket upgrade requests
server.on('upgrade', (request, socket, head) => {
  wsServer.handleUpgrade(request, socket, head, socket => {
    console.log(`[${process.env.NODE_APP_INSTANCE}] Websocket connected`);
    wsServer.emit('connection', socket, request);
  });
});

// Handle SIGINT signal (pm2)
process.on('SIGINT', async () => {
  console.log(`[${process.env.NODE_APP_INSTANCE}] SIGINT received, shutting down`);
  process.exit(0);
});

//
app.get('/api', (req, res) => {
  res.end(`Hello from instance: ${process.env.NODE_APP_INSTANCE}`);
});

console.log(`[${process.env.NODE_APP_INSTANCE}] App up, port ${port}`);
