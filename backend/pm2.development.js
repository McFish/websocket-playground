module.exports = {
  apps: [{
    name: "backend",
    script: "./src/index.ts",
    watch: true,
    "ignore_watch": ["node_modules"],
    instances: 2,
    exec_mode: "cluster"
  }]
};
