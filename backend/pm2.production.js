module.exports = {
  apps: [{
    name: "backend",
    script: "./build/index.js",
    instances: 2,
    exec_mode: "cluster"
  }]
};
